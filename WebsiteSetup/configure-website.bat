C:\Windows\System32\net.exe user UmbracoAppPool /ADD /comment:"Umbraco AppPool account" /expires:never /passwordreq:no
C:\Windows\System32\icacls.exe "C:\inetpub\Umbraco" /grant "UmbracoAppPool":(OI)(CI)F /T /inheritance:e
C:\Windows\System32\icacls.exe "C:\inetpub\Umbraco" /grant "IUSR":(OI)(CI)F /T /inheritance:e
C:\Windows\System32\icacls.exe "C:\inetpub\Umbraco" /grant "IIS_IUSRS":(OI)(CI)F /T /inheritance:e
C:\Windows\System32\inetsrv\appcmd.exe add apppool /name:Umbraco
C:\Windows\System32\inetsrv\appcmd.exe set apppool Umbraco /processModel.identityType:"SpecificUser" /processModel.userName:"UmbracoAppPool"