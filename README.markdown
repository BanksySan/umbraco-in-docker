# Setting up Umbraco in Docker for Windows

## The Docker Files

Each Docker file is compiled into one image.  The image is the blueprint of a Docker container.  When an image is run, a new container is created and the image is copied into it.

Compiling images is quite a slow process, so I've broken the requirements down into four images (three bespoke and a base image from Microsoft).  This means that if alterations are needed to the 'logic' of setting up the Umbraco image we do not have to re-compile all the other content.

### IIS With ASP.<span />NET Enabled

`iis-with-asp.DockerFile` prepares us an image with IIS installed and the ASP<span />.NET Windows optional feature installed.

### Static Content

This image just copies the static files required by Umbraco into a directory in the image/

### Umbraco

Finally we configure required users, IIS and the file system to run the application.  This is where most of the _logic_ is set.